;"use strict"
// mobile button toggle
$('.nav-button').click(function () {
	$('.nav-button .fa').toggleClass('fa-bars fa-times');
	$('.nav-button .fa').toggleClass('fa-bars-animate fa-times-animate');
	$('.nav-bar').toggleClass('nav-bar-show');
});
// плавный скроллинг 
function scrollNav() {
  $('.nav-bar a').click(function(){ 
  	// var currentLink = $('.nav-bar a').index(this);
  	// $('.active-list').removeClass('active-list');
   	// $('.nav-bar li').eq(currentLink).addClass('active-list');
   	var scrollTo =  $( $(this).attr('href') ).offset().top;
	$('html, body').stop().animate( { scrollTop: scrollTo }, 1100 );
    return false;
  });
};
scrollNav();
// mask
$("#phone").mask('+375 99 999 99 99', {placeholder:'X'});
// valid
$('#btn').click(function() {
	var name = $('#name').val();
	var phone = $('#phone').val();
	var errors = "";
	if (name.length< 4) errors += "Имя должно быть длиннее 3 сиволов ";
	if (!phone) errors += "Телефон введен неправильно! ";
   	if(name.length > 3 && phone) errors = "";
    if (errors != ""){
	  	return false;
    }else{
    	$('#showmessage').show(0);
    	$('#showmessage').html("Спасибо!");
    	$('#showmessage').hide(2000);
    	console.log('h');
    };
	jQuery.ajax({
        url:     'action_ajax_form.php', 
        type:     "POST", 
        dataType: "html", 
        data: jQuery("#ajax_form").serialize() 
    });
});
// slider
var currentSlide = 0;
function changeSlide() {
	if (currentSlide>2){
		currentSlide = 0
	}else if (currentSlide<0){
		currentSlide = 2
	};
	$('.active-slide').removeClass('active-slide');
	$('.slides').eq(currentSlide).addClass('active-slide');
	$('.active-paginator').removeClass('active-paginator');
	$('.paginator div').eq(currentSlide).addClass('active-paginator');
};
$('.next').click(function(){
	++currentSlide;
	changeSlide();
});
$('.prev').click(function(){
	--currentSlide;
	changeSlide();
});
$('.paginator div').click(function(){
	currentSlide = $('.paginator div').index(this);
	changeSlide();
});
// function: random number
function getRandomFloat(min, max) {
  return Math.random() * (max - min) + min;
};
// random color h1
$('h1').mouseenter(function() {
	var x = getRandomFloat(0, 255).toFixed(),
		y = getRandomFloat(0, 255).toFixed(),
		z = getRandomFloat(0, 255).toFixed(),
		col = "rgb("+x+","+y+","+z+")";
	$(this).css("color", col);
});
// animate track
$('#tracks').mouseenter(function(e){
	var X = e.pageX,
	    Y = e.pageY; 
    $('.snake').css({'display': 'block', 'top': Y + 'px', 'left': X+'px'});
});
$('#tracks').mouseleave(function(){
	$('.snake').css('display', 'none');
});
$('.track').mouseenter(function(e){
	var X = e.pageX,
    	Y = e.pageY; 
    $('.snake').stop().animate({'top': Y + 'px', 'left': X+'px'});
});
// animate track background
$('.track').mouseenter(function(){
	var ind = $(".track").index(this),
		ofLeft = $('.track').eq(ind).offset().left,
		ofTop = $('.track').eq(ind).offset().top,
		widthLol = $('.track').eq(ind).innerWidth(),
		heightLol = $('.track').eq(ind).innerHeight();
    $('.lol').css({ "transform": "translate("+ofLeft+"px,"+ofTop+"px)",
    				"opacity": 1,
    				"width": widthLol,
    				"height": heightLol
    			});
});
$('#tracks').mouseleave(function() {
	$('.lol').css({ "opacity": 0,
    				"width": 0,
    				"height": 0
    			});
});

// var for scrollspy+background animation
var header,discography,tracks,contact,events,story,tours,compositions;
// function: search variables
function variables() {
	header = $('#header').offset().top;
 	discography = $('#discography').offset().top;
 	tracks = $('#tracks').offset().top;
 	contact = $('#contact').offset().top;
 	events = $('#events').offset().top;
 	story = $('#story').offset().top;
 	tours = $('#tours').offset().top;
 	compositions = $('#compositions').offset().top;
};
jQuery(document).ready(function($) {
	variables();
});
	// change variables with screen resize
$(window).resize(function(){
	variables();
});
// function: change current link on scrollspy
function scrollSpy(i){
	$('.active-list').removeClass('active-list');
	$('.nav-bar li').eq(i).addClass('active-list');
};
// background animation + scrollspy
 $(window).scroll(function() {
 	// animate background
 	wind = $(document).scrollTop();
  	if (wind >= events && wind < story){
	    $('#events').css('backgroundPosition', 90*(1-events/wind)+"%"+" "+"0%"  );
	};
	if (wind >= tours && wind < compositions){
	    $('#tours').css('backgroundPosition', 20+50*(1-tours/wind)+"%"+" "+"0%"  );
	};
	// scrollspy
	if(wind >= header){
		scrollSpy(0);
	};
	if(wind >= discography){
		scrollSpy(1);
	};
	if(wind >= tours){
		scrollSpy(2);
	};
	if(wind >= compositions){
		scrollSpy(3);
	};
	if(wind >= tracks){
		scrollSpy(4);
	};
	if(wind >= events){
		scrollSpy(5);
	};
	if(wind >= story){
		scrollSpy(6);
	};
	if(wind >= contact){
		scrollSpy(7);
	};
});
// canvas
var ctx = document.getElementById('canvas').getContext('2d'),
    currentX=0,
    currentPosX,
    image = new Image();
    image.src = '/img/zoro11.png';
    setInterval(function(){
        drawImage(currentX);
        if(currentX>=3){
            currentX=0;
        }else{
            currentX++;
        }
    },500);
    function drawImage(currentX) {
        ctx.clearRect(0,0,300,200);
        currentPosX =25*currentX;
        // image.onload =
        function name(currentPosX) {
            ctx.drawImage(image,currentPosX,0,25,58,50,0,175,150);
        };
        name(currentPosX);
    };
    




 