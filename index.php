<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>My Test</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
      <!-- include -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> 

      <!--          мои стили          -->
  <link rel="stylesheet" href="style.css" >
  <link rel="stylesheet" href="media.css" >
   
</head>
<body>
  <header id="header">
    <nav >
      <div class="nav-button"><i class="fa fa-bars fa-bars-animate" aria-hidden="true"></i></div>
      <ul class="nav-bar container">
        <li class="active-list"><a href="#header">About</a></li>
        <li><a href="#discography">Discography</a></li>
        <li><a href="#tours">Concert tours</a></li>
        <li><a href="#compositions">Latter compositions</a></li>
        <li><a href="#tracks">New tracks</a></li>
        <li><a href="#events">Upcoming events</a></li>
        <li><a href="#story">History</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </nav>
    <div class="main-screen">
      <div class="main-title">
        
      </div>
    </div>
  </header>
  <section id="discography">
    <div class="container">
      <div class="section-title">
        <div></div>
        <h1>Discography</h1>
        <p>
          September 4 world heard night visions, the first full album. He reached the 2 position in the chart billboard 200. The single "It's Time" took 22 th place in the billboard hot 100. 4 th in the billboard alternative and billboard rock, and now went platinum.
        </p>
      </div>
      <div class="discography-content">
        <div class="discography-rows">
          <div>
            <span>2010</span>
            <hr>
            <h3>Hell and silence</h3>
			<canvas id="canvas"></canvas>
          </div>
          <div>
            <h3>Hell and Silence is an EP by Las Vegas rock group</h3>
            <p>
              Hell and Silence is an EP by Las Vegas rock group , released in March 2010 in the United States. It was recorded at Battle Born Studios. All songs were written by Imagine Dragons and self-produced. The EP was in part mixed by Grammy nominated engineer Mark Needham.
              To promote the album the band performed five shows at SXSW 2010 including at the BMI Official Showcase.While at SXSW they were endorsed by Blue Microphones. They also toured the western United States with Nico Vega and Saint Motel. They also performed at Bite of Las Vegas Festival 2010, New Noise Music Festival, Neon Reverb Festival, and Fork Fest.
            </p>
            <!-- <div class="button">Play</div> -->
            <audio controls>
              <source src="/aud/1.mp3">
              
              <p>Ваш браузер не поддерживает аудио</p>
            </audio>
          </div>
        </div>
        <div class="discography-rows">
          <div>
             <span>2012</span>
             <hr>
            <h3>Night visions</h3>
          </div>
          <div>
            <h3>Night Visions is the debut studio album by American rock band</h3>
            <p>
              It was released on September 4, 2012 through Interscope Records. The extended track was released on February 12, 2013, adding three more songs. Recorded between 2010 and 2012, the album was primarily produced by the band themselves, as well as English hip-hop producer Alex da Kid and Brandon Darner from the American indie rock group The Envy Corps. It was mastered by Joe LaPorta. According to frontman Dan Reynolds, the album took three years to finish ...
              of the album's tracks being previously released on multiple EPs. Musically, Night Visions exhibits influences of folk, hip hop and pop.
            </p>
            <!-- <div class="button">Play</div> -->
            <audio controls>
              <source src="/aud/1.mp3">
              
              <p>Ваш браузер не поддерживает аудио</p>
            </audio>
          </div>
        </div>
        <div class="discography-rows">
          <div>
            <span>2015</span>
            <hr>
            <h3>Smoke + mirrors</h3>
          </div>
          <div>
            <h3>The album was recorded during 2014  at the band's home studio in Las Vegas, Nevada</h3>
            <p>
              Self-produced by members of the band along with English hip-hop producer Alexander Grant, known by his moniker Alex da Kid, the album was released by Interscope Records and Grant's KIDinaKORNER label on February 17, 2015, in the United States.
            </p>
            <!-- <div class="button">Play</div> -->
            <audio controls>
              <source src="/aud/1.mp3">
              
              <p>Ваш браузер не поддерживает аудио</p>
            </audio>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="tours">
    <div class="container">
      <div class="section-title">
        <div></div>
        <h1>Concert tours</h1>
        <p>
          Before the release of Night Visions, Imagine Dragons made appearances on American radio and television to promote their extended play, Continued Silence and debut single It's Time. The band performed "It's Time" on the July 16, 2012 airing of NBC late-night talk show The Tonight Show with Jay Leno"
        </p>
      </div>
      <div class="discography-rows">
          <div>
            <span>03.08.2015</span>
            <hr>
            <h3>Smoke + mirrors tour</h3>
          </div>
          <div>
            <h3>2015 present</h3>
            <p>
              At Lollapalooza in Sao Paulo, Brazil, the last date on the Into the Night Tour, the band announced a rest, and complemented saying, "This is our last show for a while, and had no better place to end this tour".[51] The conclusion of the Into the Night Tour signaled the end of the Night Visions album cycle. Lead singer Dan Reynolds joked about the end of the Night Visions cycle, saying that "We're always writing on the road, [so] that second album will come, unless we die. 
              next year. Hopefully we don't die and there will be a second album. I don't know when it will be, but it may come"
            </p>
            <div class="button">Buy online</div>
          </div>
        </div>
    </div>
  </section>
  <section id="compositions">
    <div class="container">
      <div class="section-title">
        <div></div>
        <h1>Latter compositions</h1>
        <p>
          "It's Time" was released as the lead single from Continued Silence and It's Time, both 
          extended plays preceding Night Visions' release.
        </p>
      </div>
      <div class="compositions-content">
        <div class="compositions-rows">
          <div class="compositions-info">
            <span>03.04.2015</span>
            <hr>
            <h3>Indian summer</h3>
            <h3>Sam Feldt ft. Kimberly Anne - Show Me Love (EDX's Indian Summer Remix)</h3>
            <p>
              "Radioactive" is a song recorded by American rock band Imagine Dragons for their major-label debut EP Continued Silence and later on their debut studio album, Night Visions (2012), as the opening track. "Radioactive" was first sent to modern rock radio on October 29, 2012,[1] and released to contemporary radio on April 9, 2013. Musically, "Radioactive" is an alternative rock song with elements of electronic rock and contains cryptic lyrics about apocalyptic and revolutionist themes.
            </p>
            <div class="button">Visit the iTunes</div>
          </div>
          <div class="compositions-video">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/VI4ssGtfdxw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <div class="compositions-rows">
          <div class="compositions-video">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/VI4ssGtfdxw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
          <div class="compositions-info">
            <span>20.07.2015</span>
            <hr>
            <h3>Indian summer remix</h3>
            <h3>Sam Feldt - Show Me Love (EDX's Indian Summer Remix)</h3>
            <p>
              Amsterdam's Sam Feldt earned attention in Europe and then worldwide for his melodic house remixes, mixtapes, and collaborations including 2014's "Bloesem" with De Hofnar and "Hot Skin" with Kav Verhouzer on... Dutch label Spinnin' Records. After a series of live shows in Europe, which he built over time to contain mostly his own productions, he toured North America for the first time in early 2015. "Midnight Hearts" with the Him featuring ANG13 followed soon...
            </p>
            <div class="button">Visit the iTunes</div>
          </div>
        </div>
        <div class="compositions-rows">
          <div class="compositions-info">
            <span>03.04.2015</span>
            <hr>
            <h3>Chill nation</h3>
            <h3>James Bay - Let It Go (Bearson Remix)</h3>
            <p>
              A soulful singer/songwriter from a small English town with a penchant for crafting moving and evocative folk-pop confections in the vein of Ed Sheeran, Foy Vance, and Ben Howard, James Bay hails from the North Hertf...
              shire market town of Hitchin. Bay honed his skills regionally, eventually landing high-exposure support slots with Tom Odell, John Newman, and Kodaline before inking a record deal with Republic/Universal in 2012.
            </p>
            <div class="button">Visit the iTunes</div>
          </div>
          <div class="compositions-video">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/VI4ssGtfdxw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="slider">
    <div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
    <div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
    <div class="paginator">
      <div class="active-paginator"></div>
      <div></div>
      <div></div>
    </div>
    <div class="slides active-slide">
      <div class="slide-logo"></div><br>
      <h3>1</h3>
      <br>
      <p>
        Tolman recruited longtime high school friend Daniel Wayne "Wing" Sermon, who had graduated from Berklee College of Music. Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys. 
        Sermon then recruited another Berklee music student, Ben McKee, to join the band and complete the lineup. The band garnered a large following in their hometown of Provo, Utah, before the members moved to Las Vegas, the hometown of Dan Reynolds, where the band recorded and released their first three EPs.
      </p>
      <br>
      <p>- Early years (2008–10) -</p>
    </div>
    <div class="slides">
      <div class="slide-logo"></div><br>
      <h3>2 </h3>
      <br>
      <p>
        Tolman recruited longtime high school friend Daniel Wayne "Wing" Sermon, who had graduated from Berklee College of Music. Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys. 
        Sermon then recruited another Berklee music student, Ben McKee, to join the band and complete the lineup. The band garnered a large following in their hometown of Provo, Utah, before the members moved to Las Vegas, the hometown of Dan Reynolds, where the band recorded and released their first three EPs.
      </p>
      <br>
      <p>- Early years (2008–10) -</p>
    </div>
    <div class="slides">
      <div class="slide-logo"></div><br>
      <h3>3 </h3>
      <br>
      <p>
        Tolman recruited longtime high school friend Daniel Wayne "Wing" Sermon, who had graduated from Berklee College of Music. Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys. 
        Sermon then recruited another Berklee music student, Ben McKee, to join the band and complete the lineup. The band garnered a large following in their hometown of Provo, Utah, before the members moved to Las Vegas, the hometown of Dan Reynolds, where the band recorded and released their first three EPs.
      </p>
      <br>
      <p>- Early years (2008–10) -</p>
    </div>
  </section>
  <section id="tracks">
    <div class="container">
      <div class="wrapper">
        <div class="lol"></div>
        <div class="snake"><i class="fa fa-music" aria-hidden="true"></i>
        </div>
        <div class="track l0">
          <span>03.04.2015</span>
          <hr>
          <h3>Chill nations</h3>
          <h3>James Bay - Let It Go (Bearson Remix)</h3>
          <p>
            A soulful singer/songwriter from a small English town with a penchant for crafting moving and evocative folk-pop confections in the vein of Ed Sheeran, Foy Vance, and Ben Howard, James Bay hails from the North Hertf shire market town of Hitchin. Bay honed his skills regionally, eventually landing high-exposure support slots with Tom Odell, John Newman, and Kodaline before inking a record deal with Republic/Universal in 2012.
          </p>
        </div>
        <div class="track l1">
          <span>01.03.2015</span>
          <hr>
          <h3>Chill nations</h3>
          <h3>Smoke and mirrors</h3>
          <p>
            Hard Rock Cafe teamed with the band, granting them the first ever full access to take control of Hard Rock Cafe's internal video system (more than 20,000 screens at all 151 locations worldwide) on February 17, 2014. Also, autographed t-shirts will be hidden throughout the Hard Rock Cafe shops supporting the Tyler Robinson Foundation, benefiting the families of children with cancer.
          </p>
        </div>
        <div class="track l2">
          <span>04.04.2015</span>
          <hr>
          <h3>Chill nations</h3>
          <h3>It Comes Back to You</h3>
          <p>
            In 2015 a world tour, entitled the Smoke and Mirrors Tour is scheduled in promotion of the world-wide release of Smoke and Mirrors.
          </p>
        </div>
        <div class="track l3">
          <span>01.07.2015</span>
          <hr>
          <h3>Chill nations</h3>
          <h3>Let It Go (Bearson Remix)</h3>
          <p>
            A soulful singer/songwriter from a small English town with a penchant for crafting moving and evocative folk-pop confections in the vein of Ed Sheeran, Foy Vance, and Ben Howard, James Bay hails from the North Hertf shire market town of Hitchin. Bay honed his skills regionally, eventually landing high-exposure support slots with Tom Odell, John Newman, and Kodaline before inking a record deal with Republic/Universal in 2012.
          </p>
        </div>
        <div class="track l4">
          <span>02.11.2015</span>
          <hr>
          <h3>Chill nations</h3>
          <h3>I Don't Mind</h3>
          <p>
            Hell and Silence is an EP by Las Vegas rock group Imagine Dragons, released in March 2010 in the United States. It was recorded at Battle Born Studios. All songs were written by Imagine Dragons and self-produced. The EP was in part mixed by Grammy nominated engineer Mark Needham.
            To promote the album the band performed five shows at SXSW 2010 including at the BMI Official Showcase. While at SXSW they were endorsed by Blue Microphones.
          </p>
        </div>
        <div class="track l5">
          <span>06.09.2015</span>
          <hr>
          <h3>Chill nations</h3>
          <h3>Smoke + Mirrors</h3>
          <p>
            A soulful singer/songwriter from a small English town with a penchant for crafting moving and evocative folk-pop confections in the vein of Ed Sheeran, Foy Vance, and Ben Howard, James Bay hails from the North Hertf shire market town of Hitchin. Bay honed his skills regionally, eventually landing high-exposure support slots with Tom Odell, John Newman, and Kodaline before inking a record deal with Republic/Universal in 2012.
          </p>
        </div>
      </div>
    </div>
  </section>
  <section id="events">
    <div class="container">
      <div class="event">
        <div class="section-title">
          <div></div>
          <h1>Upcoming events</h1>
        </div>
        <div class="event-info">
          <h3>Smoke + Mirrors Tour</h3>
          <hr>
          <p>2-2-15 Hokukoryokuchi, Konohana Ward,<br> 554-0042 Osaka Prefecture</p>
          <div class="line"></div>
          
          <span>01.04.2015</span>
        
          <div class="line"></div>
          
          <h3>Smoke + Mirrors Tour</h3>
          <hr>
          <p>
            When the last-minute request is for a focus group, it’s usually a sign that the
            request originated in Marketing. When Web sites are being designed, the folks in
            Marketing often feel like they don’t have much clout. Even though they’re the
            ones who spend the most time trying to figure out who the site’s audience is and
            what they want, the designers and developers are the ones with most of the
            hands-on control over how the site actually gets put together.
          </p>
           <div class="line"></div>
          <span>27.05.2015</span>
        </div>
      </div>
    </div>
  </section>
  <section id="story">
    <div class="container">
      <div class="section-title">
        <div></div>
        <h1>History</h1>
        <p>
          In 2008, lead singer Dan Reynolds met drummer Andrew Tolman at Brigham Young University where they were both students.Tolman recruited longtime high school friend Daniel Wayne "Wing" Sermon, who had graduated from Berklee College of Music
        </p>
      </div>
      <div class="history-content">
        <h3>May 09</h3>
        <p>Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.</p>
      </div>
    </div>
  </section>
  <footer id="contact">
    <div class="container">
      <div class="contact">
        <div class="section-title">
          <div></div>
          <h1>Contact</h1>
          <p>Canada Island, Division No. 23, Unorganized, MB, Canada</p>
          <br>
          <p>Tel. +1(778) 288 5180</p>  
        </div>
        
        <form method="post" id="ajax_form">
          <input type="text" name="name" id="name" placeholder="Name *" required=""><br>
          <input type="text" name="phone" id="phone" placeholder="Phone *" required=""><br>
         <!--  <input type="text" name="email" id="email" placeholder="Email"> -->
          <br>
          <input id="btn"  type="button" value="SEND">
          <div id="showmessage"> </div>
        </form>
        <div class="socials">
          <span>
            <a href="https://www.facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a>
          </span>
          <br><br>
          <span>
            <a href="https://www.instagram.com"><i class="fa fa-instagram" aria-hidden="true"></i></a>
          </span>
          <br><br>
          <span>
            <a href="https://twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          </span>
        </div>
      </div>
    </div>
  </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="jquery.maskedinput.js"></script>
  <script src="js.js"></script>
</body>
</html>